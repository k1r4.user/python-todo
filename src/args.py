import argparse


def parse_args(args):
    parser = argparse.ArgumentParser()

    composing = parser.add_argument_group(("Writing new todos", "чтобы добавить новую запись в туду лист необходимо выполнить Х"))
    composing.add_argument("text", metavar="", nargs="*")

    standalone = parser.add_argument_group(
        "Standalone Commands",
        "После выполнения этих команд работы программы завершиться",
    )
    standalone.add_argument(
        "--delete",
        dest="delete",
        help="Удаляеть запись по индексу",
    )

    return parser.parse_known_args(args)
