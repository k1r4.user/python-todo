import json
import sys
# TODO



class TodoJournal:
    def __init__(self, path_todo):
        """
        Функция инициализации
        :param path_todo:
        """
        self.path_todo = path_todo
        self.name = self._parse()["name"]
        self.entries = self._parse()["todos"]
        data = self._parse()
        self.date_todo = data["date_todo"]

    def __len__(self):
        """
        Функция подсчета записей
        :return: количество записей
        """
        return len(self.entries)

    def __iter__(self):
        """
        Функция итерации файла
        :return: извлеченные записи в последовательности
        """
        return iter(self.entries)

    def get_entry(self, index):
        return self.entries[index]

    @staticmethod
    def create(filename, name):
        """
        Функция создает файл с тудушкой с заданным именем
        :param filename: Путь до создаваемой тудушки
        :param name: Имя создаваемой тудушки
        :return:
        """
        with open(filename, "w") as todo_file:
            json.dump(
                {"name": name, "todos": [], "date_todo": []},
                todo_file,
                sort_keys=True,
                indent=4
            )

    def add_entry(self, new_entry):
        """
        Функция добавления записей в файл
        :param new_entry: создаваемая запись
        :return:
        """
        self.entries.append(new_entry)

        new_data = {
            "name": self.name,
            "todos": self.entries,
        }

        self._update(new_data)

    def remove_entry(self, index):
        """
        Функция удаляет из файла имеющиеся записи
        :param index: индекс удаляемой записи
        :return:
        """
        data = self._parse()
        name = data["name"]
        todos = data["todos"]

        todos.remove(todos[index])

        new_data = {
            "name": name,
            "todos": todos,
        }

        self._update(new_data)

    def _parse(self):
        try:
            with open(self.path_todo, 'r') as todo_file:
                data = json.load(todo_file)
            return data
        except FileNotFoundError:
            print(f"Не существует тудушки с таким путем {self.path_todo}")
            sys.exit(1)

    def _update(self, new_data):
        with open(self.path_todo, "w", encoding='utf-8') as todo_file:
            json.dump(
                new_data,
                todo_file,
                sort_keys=True,
                indent=4,
                ensure_ascii=False,
            )
