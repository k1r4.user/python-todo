from src.todo_journal import TodoJournal

def run(args):
    todo = TodoJournal("src/test")

    # TODO рефакторинг работы по индексам
    if args[0].text:
        # TODO пофиксить баг добавления тудушки без пробелов
        raw_text = ''.join(args[0].text)
        todo.add_entry(raw_text)
    if args[0].delete:
        todo.remove_entry(int(args[0].delete))
