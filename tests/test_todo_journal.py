import json

from src.todo_journal import TodoJournal


def test_init():
    #GIVEN
    TodoJournal.create("test_todo", "todoha")
    todo = TodoJournal("test_todo")
    entries = todo.entries
    name = todo.name
    #THEN
    expected_entries = []
    expected_name = "test todo"
    #NEXT
    assert entries == expected_entries


def test_create_journal(tmpdir):
    # GIVEN
    expected_todo = json.dumps(
        {
            "name": "test_name",
            "todos": []
        },
        indent=4)

    # THEN
    todo_filename = "test_todo"
    todo_journal = tmpdir.join(todo_filename)
    TodoJournal.create(todo_journal, "test_name")
    read_todo = todo_journal.read()
    # NEXT
    assert expected_todo == read_todo


def test_add_entry(tmpdir):
    todo_filename = "test_todo"
    todo = tmpdir.join(todo_filename)
    TodoJournal.create(todo, "test")
    todo_jrnl = TodoJournal(todo)
    todo_jrnl.add_entry("Сходить за молоком")

    expected_todo = json.dumps(
        {
            "name": "test",
            "todos": ["Сходить за молоком"]
        },
        indent=4,
        ensure_ascii=False, )
    assert expected_todo == todo.read()

def test_remove_entry(todo_json_after_remove_second_entry, todo_object_with_3_entries):
    #GIVEN
    todo_object_with_3_entries.remove_entry(1)
    after_remove = todo_object_with_3_entries.path_todo.read()


    #THEN
    expected_todo_json_after_remove_second_entry = todo_json_after_remove_second_entry

    #NEXT

    assert after_remove == expected_todo_json_after_remove_second_entry

def test_len_and_add():
    TodoJournal.create("test_la", "todo")
    todo = TodoJournal("test_la")
    todo.add_entry("qwerty")
    todo.add_entry("12345")
    todo.add_entry("priv")
    assert len(todo) == 3

def test_iter():
    #GIVEN
    TodoJournal.create("test_iter", "todo`er")
    todo = TodoJournal("test_iter")
    # THEN
    todo.add_entry('qwerty')
    todo.add_entry('12345')
    todo.add_entry('Ctrl')
    todo.add_entry('Alt')
    todo.add_entry('Delete')
    my_iter = iter(todo)
    out1 = next(my_iter)
    out2 = next(my_iter)
    out3 = next(my_iter)
    out4 = next(my_iter)
    out5 = next(my_iter)
    #NEXT
    assert out1 == 'qwerty'
    assert out2 == '12345'
    assert out3 == 'Ctrl'
    assert out4 == 'Alt'
    assert out5 == 'Delete'