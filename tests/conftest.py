import json
import pytest

from src.todo_journal import TodoJournal


@pytest.fixture()
def todo_journal_with_3_entries(tmpdir):
    todo_filename = "test_todo"
    todo_path = tmpdir.join(todo_filename)
    with open(todo_path, "w") as f:
        json.dump(
            {
                "name": "test",
                "todos": ["first entry", "second_entry", "third entry"]
            },
            f,
            indent=4,
            ensure_ascii=False, )
    return todo_path


@pytest.fixture()
def todo_json_after_remove_second_entry():
    return json.dumps(
        {
            "name": "test",
            "todos": ["first entry", "third entry"]
        },
        indent=4,
        ensure_ascii=False, )

@pytest.fixture()
def todo_object_with_3_entries(todo_journal_with_3_entries):
    return TodoJournal(todo_journal_with_3_entries)

